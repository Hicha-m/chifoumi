###########################################
#
# Le chifoumi contre l'ordinateur
#
#
#
###########################################

import random  # pour des choix aléatoires

# choix possibles dans une dictionaire : pierre, papier, ciseaux

dict_pcf = {'pierre': {'alias': '0' , 'win' : '1'}, 'ciseaux': {'alias': '1' , 'win' : '2'}, 'feuille': {'alias': '2' , 'win' : '0'}}
dict_alias = {}

# nombre maximum de victoires pour qu'il y ait un gagnant
nombre_max_victoires = 3

# vos fonctions


def faire_choisir_ordinateur():
    """faire_choisir_ordinateur : On doit faire choisir l'ordinateur
    entre les choix possibles de manière aléatoire (choisir une fonction du module random)
    retourner le choix trouvé"""
    print("C'est au tour de l'ordinateur de choisir !")
    choix = random.choice(list(dict_alias))
    return int(choix)


def demander_choix_joueur():
    """demander_choix_joueur : demander au joueur quel est son choix et vérifier
    qu'il fait partie des choix possibles
    retourner le choix du joueur"""
    print("C'est au tour du joueur de choisir !")
    choix = input('Choisissez votre coup : ')
    if choix in dict_pcf:
        return int(dict_pcf[choix]['alias'])
    elif choix in list(dict_alias):
        return int(choix)
    else:
        print("Votre choix est incorecte.\nVeuillez réessayer parmis ces propositions : {0}.\n".format([x + '=' + dict_pcf[x]['alias'] for x in list(dict_pcf)]))
        return demander_choix_joueur()


def afficher_choix(chx_joueur, chx_ordinateur):
    """afficher_choix : afficher le choix du joueur et de l'ordinateur
    sous forme textuelle (pierre, papier, ...).
    ne retourne rien"""
    choix = [chx_joueur, chx_ordinateur]

    for x in range(0, len(choix)):
        if choix[x] == 0:
            choix[x] = ("Pierre")
        elif choix[x] == 1:
            choix[x] = ("Papier")
        else:
            choix[x] = ("Ciseaux")

    print("Le joueur a choisi :", choix[0],
          ",et l'ordiniateur a choisi :", choix[1])


def comparer_choix(chx_joueur, chx_ordinateur):
    """comparer_choix : Avec une série de conditions, comparer les choix
    et déterminer le gagnant
    retourner le gagnant du tour"""
    choix = [str(chx_joueur), str(chx_ordinateur)]


    if dict_alias[choix[1]]['win'] == choix[0]:
        gagnant = "Le joueur"
    elif dict_alias[choix[0]]['win'] == choix[1]:
        gagnant = "L'ordinateur"
    else:
        gagnant = "Aucun"
        
    return gagnant


def mettre_score_a_jour(gagnant, scr_joueur, scr_ordinateur):
    """mettre_score_a_jour : Mettre les scores à jour en fonction
    du gagnant du tour.
    retourner si il y a un gagnant pour la partie, le score actualisé du joueur, le score actualisé de l'ordinateur"""

    if gagnant == "Le joueur":
        scr_joueur += 1
        print('Le joueur a remporté ce tours !')
    elif gagnant == "L'ordinateur":
        scr_ordinateur += 1
        print('L\'ordinateur a remporté ce tours !')
    elif gagnant == "Aucun":
        print('Personne a gagné ce tours !')

    if scr_joueur == 3 or scr_ordinateur == 3:
        pas_de_gagnant = False
    else:
        pas_de_gagnant = True

    return pas_de_gagnant, scr_joueur, scr_ordinateur


def afficher_scores(scr_joueur, scr_ordinateur):
    """afficher_score : afficher les scores
    ne retourne rien"""
    print("SCORE : (Joueur : ", scr_joueur,
          " Ordinateur : ", scr_ordinateur, ")\n", sep='')


def determiner_gagnant(scr_joueur, scr_ordinateur):
    """déterminer le gagnant en comparant le score du joueur et de l'ordinateur
    grace au nombre_max_victoires, retourner le gagnant
    """

    if scr_joueur == 3:
        gagnant = "Joueur"

    if scr_ordinateur == 3:
        gagnant = "Ordinateur"

    return gagnant


# le programme principal
tour = 0  # le tour actuel de jeu
score_joueur = 0  # le score du joueur
score_ordinateur = 0  # le score de l'ordinateur

"""
# Paramètre de la partie
# partie multiple / partie bot vs bot / partie spoke / partie entre plusieur joueurs (plus de deux) & bot a voir si j'arrive /
multiple = False
bot = False
addPlayer = False # a voir / systeme de nom des joueur possible avec une class pour le faire
addBot = False # a voir / la meme avec la class 
modeS = False # mode Spoke etc... a choisir

fonction a faire

changé le mot ciseau en ascii etc...
"""


parametre = input('Voulez-vous changer les paramètre par défault')

if parametre == 'Oui':
   print('Paramètre :\n',  
        '   multiple (1-99) : Partie multiple avec tant de manches.\n',
        '   bot : Partie automatique avec deux bot\n',
        '   setPlayer (1-5) : Choisir les joueurs dans la partie\n',
        '   setBot (1-5) : Choisir les bots dans la partie | max 6 joueurs ou bots\n',
        '   modeS : Changement du mode en Spoke')     
   
   answer = input()

# dictionaire dict_alias
for key in dict_pcf:
    alias = dict_pcf[key]['alias']
    win = dict_pcf[key]['win']
    dict_alias[alias] = {}
    dict_alias[alias]['win'] = win


# pas de gagnant == True (Vrai) : il n'y a pas encore de gagnant
# (False : il y a un gagnant)
pas_de_gagnant = True
while pas_de_gagnant:
    choix_joueur = demander_choix_joueur()
    choix_ordinateur = faire_choisir_ordinateur()
    afficher_choix(choix_joueur, choix_ordinateur)
    gagnant_tour = comparer_choix(choix_joueur, choix_ordinateur)
    pas_de_gagnant, score_joueur, score_ordinateur = mettre_score_a_jour(
        gagnant_tour, score_joueur, score_ordinateur)
    afficher_scores(score_joueur, score_ordinateur)
    tour += 1
gagnant_final = determiner_gagnant(score_joueur, score_ordinateur)
print("Le gagnant est :", gagnant_final, "au bout de ", tour, "tours")
print("Merci d'avoir joué !")
