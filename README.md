# Modèle de projet pour chifoumi

![alt text](https://gitlab.com/Hicha-m/chifoumi/-/raw/master/image.png?raw=true)

Travail de base:

Compléter chaque fonction vide (remplacer "pass") par le code permettant de répondre aux indications présentes dans les commentaires de chaque fonction
Penser à bien documenter vos lignes de code pour produire un code explicite.
Aller plus loin (au choix):

Intégrer une autre version (comme avec le lézard et spock).
Essayer de faire une représentation des choix pour la fonction afficher choix. Voir ascii art sur google...
Modifier le programme pour intégrer les parties multiples (score total, score de la partie, gagnant du jeu, ...).

![alt text](https://gitlab.com/Hicha-m/chifoumi/-/raw/master/PierrePapierCiseauxLezardSpock.jpg?raw=true)
